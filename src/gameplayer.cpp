// (c) 2016 - 2017: Marcel Admiraal

#include "gameplayer.h"
#include "gamepolicy.h"

#include "mcmclearner.h"
#include "netlearner.h"
#include "tdnetlearner.h"

namespace ma
{
    GamePlayer::GamePlayer(const unsigned int playerId) :
        Predictor(), playerId(playerId), automated(false)
    {
        newMCMCLearner(2, 1);
        learningPolicy = new EGreedyGamePolicy();
        notLearningPolicy = new GreedyGamePolicy();
    }

    GamePlayer::~GamePlayer()
    {
        delete learningPolicy;
        delete notLearningPolicy;
    }

    GameAction* GamePlayer::getNextAction(const Game* game)
    {
        List<GameAction*> possibleActions = game->getPossibleActions();
        GameAction* nextAction = 0;
        if (isLearning())
        {
            nextAction = learningPolicy->selectAction(game, this,
                    possibleActions);
        }
        else
        {
            nextAction = notLearningPolicy->selectAction(game, this,
                    possibleActions);
        }
        nextAction = nextAction->createCopy();
        for (Iterator<GameAction*> iter = possibleActions.first(); iter.valid();
                ++iter) delete (*iter);
        return nextAction;
    }

    void GamePlayer::nextState(const Game* game)
    {
        Predictor::nextState(getGameState(game));
    }

    void GamePlayer::gameOver(const Game* game)
    {
        Predictor::actualResult(getResult(game));
    }

    void GamePlayer::newGame()
    {
        Predictor::sequenceReset();
    }

    unsigned int GamePlayer::getPlayerId() const
    {
        return playerId;
    }

    void GamePlayer::setAutomated(bool automated)
    {
        this->automated = automated;
    }

    bool GamePlayer::isAutomated() const
    {
        return automated;
    }

    void GamePlayer::setLearningPolicy(const GamePolicy& learningPolicy)
    {
        this->learningPolicy = learningPolicy.createCopy();
    }

    void GamePlayer::setNotLearningPolicy(const GamePolicy& learningPolicy)
    {
        this->notLearningPolicy = learningPolicy.createCopy();
    }

    double GamePlayer::getGameValue(const Game* game)
    {
        return getPrediction(getGameState(game))[0];
    }

    Vector GamePlayer::getGameState(const Game* game) const
    {
        return Vector(getStateLength());
    }

    Vector GamePlayer::getResult(const Game* game) const
    {
        Vector result(getPredictionLength());
        if (game->getNextPlayer() == 0)
        {
            if (game->getWinner() == 0) result[0] = 0.5;
            if (game->getWinner() == playerId) result[0] = 1;
        }
        return result;
    }
}
