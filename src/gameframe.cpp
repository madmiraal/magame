// (c) 2016 - 2017: Marcel Admiraal

#include "gameframe.h"

#include "vector.h"
#include "icons.h"

#include <wx/menu.h>
#include <wx/toolbar.h>
#include <wx/msgdlg.h>
#include <wx/filedlg.h>
#include <wx/textdlg.h>
#include <wx/choicdlg.h>

namespace ma
{
    GameFrame::GameFrame(
            Game* game, wxWindow* parent, const wxWindowID id,
            const Str& title, const wxPoint& position, const wxSize& size,
            const long style, const wxString& name) :
        ControlFrame(new GameControl(game), parent, id, title, position, size,
                style, name),
        gameControl((GameControl*)control), pauseTool(0), continueTool(0)
    {
        // Create the menus.
        unsigned int players = gameControl->getPlayers();
        unsigned int thisId = 10;
        wxMenu* fileMenu = new wxMenu();
        fileMenu->Append(wxID_EXIT, wxT("&Quit"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GameFrame::quit, this, wxID_EXIT);
        menuBar->Append(fileMenu, wxT("&File"));

        wxMenu* learningMenu = new wxMenu();
        learningMenu->Append(++thisId, wxT("New Learner"),
                wxT("Create a new learner for a player"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GameFrame::newLearner, this, thisId);
        learningMenu->Append(++thisId, wxT("Load Learner"),
                wxT("Load a saved learner for a player"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GameFrame::loadLearner, this, thisId);
        learningMenu->Append(++thisId, wxT("Save Learner"),
                wxT("Save the learner for a player"));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GameFrame::saveLearner, this, thisId);
        menuBar->Append(learningMenu, wxT("Learning"));
        learningMenu->Append(++thisId, wxT("Set Learning Rate"),
                wxT("Set the learner's learning rate."));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GameFrame::setLearningRate, this,
                thisId);
        learningMenu->Append(++thisId, wxT("Set Discount Rate"),
                wxT("Set the learner's discount rate."));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GameFrame::setDiscountRate, this,
                thisId);

        wxMenu* settingsMenu = new wxMenu();
        autoId = thisId;
        for (unsigned int player = 1; player <= players; ++player)
        {
            wxString itemText("Automate Player ");
            itemText << player;
            wxString helpText("Whether or not Player ");
            helpText << player;
            helpText << " will be played by the computer.";
            settingsMenu->AppendCheckItem(++thisId, itemText, helpText);
            Bind(wxEVT_COMMAND_MENU_SELECTED, &GameFrame::automatePlayer, this, thisId);
            settingsMenu->Check(thisId, gameControl->isAutomated(player));
        }
        settingsMenu->AppendCheckItem(++thisId, wxT("Automate Restart"),
                wxT("Automatically starts a new game when the game finishes."));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GameFrame::automateRestart, this, thisId);
        settingsMenu->Check(thisId, gameControl->isAutomatedReset());
        settingsMenu->AppendSeparator();
        learnId = thisId;
        for (unsigned int player = 1; player <= players; ++player)
        {
            wxString itemText("Player ");
            itemText << player;
            itemText << " learning";
            wxString helpText("Whether or not learning is enabled for player ");
            helpText << player;
            settingsMenu->AppendCheckItem(++thisId, itemText, helpText);
            Bind(wxEVT_COMMAND_MENU_SELECTED, &GameFrame::setLearning, this, thisId);
            settingsMenu->Check(thisId, gameControl->isLearning(player));
        }
        settingsMenu->AppendCheckItem(++thisId, wxT("Slow"),
                wxT("Slows the automatic play for human viewing."));
        Bind(wxEVT_COMMAND_MENU_SELECTED, &GameFrame::setSlow, this, thisId);
        settingsMenu->Check(thisId, gameControl->isSlow());
        menuBar->Append(settingsMenu, wxT("&Settings"));
        SetMenuBar(menuBar);

        // Create the toolbar.
        wxImage::AddHandler(new wxPNGHandler);
        pauseId = ++thisId;
        toolBar->AddTool(pauseId, wxT("Pause"), pauseIcon,
                wxT("Pause game"));
        Bind(wxEVT_COMMAND_TOOL_CLICKED, &GameFrame::switchPause, this, pauseId);
        continueId = ++thisId;
        toolBar->AddTool(continueId, wxT("Continue"), goSignIcon,
                wxT("Continue game"));
        Bind(wxEVT_COMMAND_TOOL_CLICKED, &GameFrame::switchPause, this, continueId);
        toolBar->AddTool(++thisId, wxT("Restart"), restartIcon,
                wxT("Restart game"));
        Bind(wxEVT_COMMAND_TOOL_CLICKED, &GameFrame::reset, this, thisId);
        toolBar->Realize();
        if (gameControl->isLoopPaused())
            pauseTool = toolBar->RemoveTool(pauseId);
        else
            continueTool = toolBar->RemoveTool(continueId);

        Bind(wxEVT_CLOSE_WINDOW, &GameFrame::close, this);
    }

    GameFrame::~GameFrame()
    {
        if (continueTool != 0) delete continueTool;
        if (pauseTool != 0) delete pauseTool;
    }

    void GameFrame::loadLearners()
    {
        if (!gameControl->isLoopPaused()) switchPause();
        wxMessageDialog loadDialog(this, wxT("Load Previously Saved Learning?"),
                wxT("Load Learning?"), wxYES_NO);
        if (loadDialog.ShowModal() == wxID_YES)
        {
            for (unsigned int playerId = 1; playerId <= 2; ++playerId)
            {
                loadLearner(playerId);
            }
        }
        switchPause();
    }

    unsigned int GameFrame::getPlayerId(const wxString& caption)
    {
        unsigned int players = gameControl->getPlayers();
        if (players == 1) return 1;
        wxString playerString[players];
        for (unsigned int player = 0; player < players; ++player)
        {
            playerString[player] << (player + 1);
        }
        wxSingleChoiceDialog playerIdDialog(this,
                wxT("Select player"), caption, players,
                playerString);
        playerIdDialog.SetSize(330, 220);
        if (playerIdDialog.ShowModal() == wxID_CANCEL) return 0;
        return playerIdDialog.GetSelection() + 1;
    }

    void GameFrame::newLearner(wxCommandEvent& event)
    {
        bool paused = gameControl->isLoopPaused();
        if (!paused) switchPause();
        int playerId = getPlayerId(wxString("Create New Learner"));
        if (playerId != 0) newLearner(playerId);
        if (!paused) switchPause();
    }

    void GameFrame::newLearner(unsigned int playerId)
    {
        wxString choice[3];
        choice[0] = wxT("MCMC Learner");
        choice[1] = wxT("MC Net Learner");
        choice[2] = wxT("TD Net Learner");
        wxString caption("Create New Learner for Player ");
        caption << playerId;
        wxSingleChoiceDialog learnerDialog(this, wxT("Select the type of learner:"),
                caption, 3, choice);
        learnerDialog.SetSize(330, 220);
        if (learnerDialog.ShowModal() == wxID_CANCEL) return;

        Vector layerSize;
        double learnRate, discountRate, defaultValue, gamma, lambda;
        NeuronType hiddenType = NeuronType::Normal;
        switch (learnerDialog.GetSelection())
        {
        case 0:
            learnRate = getLearnRate(caption, 0.1);
            if (std::isnan(learnRate)) return;
            discountRate = getDiscountRate(caption, 1);
            if (std::isnan(discountRate)) return;
            defaultValue = getDefaultValue(caption, 0.5);
            if (std::isnan(defaultValue)) return;
            gameControl->newMCMCLearner(playerId, learnRate, discountRate,
                    defaultValue);
            break;
        case 1:
            layerSize = getHiddenLayerSizes(caption,
                    gameControl->getGamePlayer(playerId)->getStateLength() * 2);
            if (layerSize.size() == 0) return;
            hiddenType = getHiddenType(caption);
            if (hiddenType == NeuronType::Normal) return;
            learnRate = getLearnRate(caption, 0.001);
            if (std::isnan(learnRate)) return;
            discountRate = getDiscountRate(caption, 1);
            if (std::isnan(discountRate)) return;
            gamma = getGamma(caption, 0.001);
            if (std::isnan(gamma)) return;
            gameControl->newMCNetLearner(playerId, layerSize, hiddenType,
                    learnRate, discountRate, gamma);
            break;
        case 2:
            layerSize = getHiddenLayerSizes(caption,
                    gameControl->getGamePlayer(playerId)->getStateLength() * 2);
            if (layerSize.size() == 0) return;
            hiddenType = getHiddenType(caption);
            if (hiddenType == NeuronType::Normal) return;
            learnRate = getLearnRate(caption, 0.001);
            if (std::isnan(learnRate)) return;
            discountRate = getDiscountRate(caption, 1);
            if (std::isnan(discountRate)) return;
            gamma = getGamma(caption, 0.001);
            if (std::isnan(gamma)) return;
            lambda = getLambda(caption, 0.7);
            if (std::isnan(lambda)) return;
            gameControl->newTDNetLearner(playerId, layerSize, hiddenType,
                    learnRate, discountRate, gamma, lambda);
            break;
        default:
            return;
        }
    }

    void GameFrame::loadLearner(wxCommandEvent& event)
    {
        bool paused = gameControl->isLoopPaused();
        if (!paused) switchPause();
        int playerId = getPlayerId(wxString("Load Learner"));
        if (playerId != 0) loadLearner(playerId);
        if (!paused) switchPause();
    }

    void GameFrame::loadLearner(unsigned int playerId)
    {
        wxString filename("player");
        filename << playerId << ".dat";
        wxString windowTitle("Load Player ");
        windowTitle << playerId << " Learning";
        bool success = false;
        while (!success)
        {
            wxFileDialog openFileDialog(this, windowTitle, wxEmptyString,
                    filename, wxFileSelectorDefaultWildcardStr,
                    wxFD_OPEN|wxFD_FILE_MUST_EXIST);
            if (openFileDialog.ShowModal() == wxID_CANCEL) return;
            success = gameControl->loadLearning(
                    playerId, openFileDialog.GetPath());
            if (!success)
            {
                wxMessageBox(wxT("Load Failed"));
            }
        }
    }

    void GameFrame::saveLearners()
    {
        if (!gameControl->isLoopPaused()) switchPause();
        wxMessageDialog saveDialog(this, wxT("Save Learning?"),
                wxT("Save Learning?"), wxYES_NO);
        if (saveDialog.ShowModal() == wxID_YES)
        {
            for (unsigned int playerId = 1; playerId <= 2; ++playerId)
            {
                saveLearner(playerId);
            }
        }
    }

    void GameFrame::saveLearner(wxCommandEvent& event)
    {
        bool paused = gameControl->isLoopPaused();
        if (!paused) switchPause();
        int playerId = getPlayerId(wxString("Save Learner"));
        if (playerId != 0) saveLearner(playerId);
        if (!paused) switchPause();
    }

    void GameFrame::saveLearner(unsigned int playerId)
    {
        wxString filename("player");
        filename << playerId << ".dat";
        wxString windowTitle("Save Player ");
        windowTitle << playerId << " Learning";
        bool success = false;
        while (!success)
        {
            wxFileDialog saveFileDialog(this, windowTitle, wxEmptyString,
                    filename, wxFileSelectorDefaultWildcardStr,
                    wxFD_SAVE|wxFD_OVERWRITE_PROMPT);
            if (saveFileDialog.ShowModal() == wxID_CANCEL) return;
            success = gameControl->saveLearning(
                    playerId, saveFileDialog.GetPath());
            if (!success)
            {
                wxMessageBox(wxT("Save Failed"));
            }
        }
    }

    void GameFrame::setLearningRate(wxCommandEvent& event)
    {
        bool paused = gameControl->isLoopPaused();
        if (!paused) switchPause();
        int playerId = getPlayerId(wxString("Set Learning Rate"));
        if (playerId != 0) setLearningRate(playerId);
        if (!paused) switchPause();
    }

    void GameFrame::setLearningRate(unsigned int playerId)
    {
        wxString rateString;
        rateString << gameControl->getLearningRate(playerId);
        bool valid = false;
        double newRate;
        while (!valid)
        {
            wxTextEntryDialog numberDialog (this, wxT("Learning Rate:"),
                    wxT("Set Learning Rate"), rateString);
            if (numberDialog.ShowModal() == wxID_CANCEL) return;
            wxString newRateString = numberDialog.GetValue();
            valid = newRateString.ToDouble(&newRate) &&
                    newRate >= 0 && newRate <= 1;
            if (!valid)
            {
                wxMessageDialog error (this,
                        wxT("Must be a double between 0 and 1."),
                        wxT("Invalid value!"));
                error.ShowModal();
            }
        }
        gameControl->setLearningRate(playerId, newRate);
    }

    void GameFrame::setDiscountRate(wxCommandEvent& event)
    {
        bool paused = gameControl->isLoopPaused();
        if (!paused) switchPause();
        int playerId = getPlayerId(wxString("Set Discount Rate"));
        if (playerId != 0) setDiscountRate(playerId);
        if (!paused) switchPause();
    }

    void GameFrame::setDiscountRate(unsigned int playerId)
    {
        wxString rateString;
        rateString << gameControl->getDiscountRate(playerId);
        bool valid = false;
        double newRate;
        while (!valid)
        {
            wxTextEntryDialog numberDialog (this, wxT("Discount Rate:"),
                    wxT("Set Discount Rate"), rateString);
            if (numberDialog.ShowModal() == wxID_CANCEL) return;
            wxString newRateString = numberDialog.GetValue();
            valid = newRateString.ToDouble(&newRate) &&
                    newRate >= 0 && newRate <= 1;
            if (!valid)
            {
                wxMessageDialog error (this,
                        wxT("Must be a double between 0 and 1."),
                        wxT("Invalid value!"));
                error.ShowModal();
            }
        }
        gameControl->setDiscountRate(playerId, newRate);
    }

    void GameFrame::automatePlayer(wxCommandEvent& event)
    {
        int playerId = event.GetId() == autoId + 1 ? 1 : 2;
        gameControl->setAutomated(playerId, event.IsChecked());
    }

    void GameFrame::automateRestart(wxCommandEvent& event)
    {
        gameControl->setAutomatedReset(event.IsChecked());
    }

    void GameFrame::setLearning(wxCommandEvent& event)
    {
        int playerId = event.GetId() == learnId + 1 ? 1 : 2;
        gameControl->setLearning(playerId, event.IsChecked());
    }

    void GameFrame::setSlow(wxCommandEvent& event)
    {
        gameControl->setSlow(event.IsChecked());
    }

    void GameFrame::reset(wxCommandEvent& event)
    {
        gameControl->newGame();
    }

    void GameFrame::switchPause(wxCommandEvent& event)
    {
        switchPause();
    }

    void GameFrame::switchPause()
    {
        bool paused = gameControl->isLoopPaused();
        if (paused && continueTool == 0)
        {
            continueTool = toolBar->RemoveTool(continueId);
            toolBar->InsertTool(0, pauseTool);
            pauseTool = 0;
        }
        if (!paused && pauseTool == 0)
        {
            pauseTool = toolBar->RemoveTool(pauseId);
            toolBar->InsertTool(0, continueTool);
            continueTool = 0;
        }
        gameControl->pauseLoop(!paused);
    }

    void GameFrame::close(wxCloseEvent& event)
    {
        saveLearners();
        event.Skip();
    }

    void GameFrame::quit(wxCommandEvent& event)
    {
        Close(false);
    }

    Vector GameFrame::getHiddenLayerSizes(const wxString& caption,
            const double defaultValue)
    {
        unsigned int layers = 0;
        while (layers == 0)
        {
            wxString defaultString; defaultString << 1;
            wxTextEntryDialog layerDialog(this,
                    wxT("Enter number of hidden layers:"), caption,
                    defaultString);
            layerDialog.SetTextValidator(wxFILTER_DIGITS);
            if (layerDialog.ShowModal() == wxID_CANCEL) return Vector();
            layers = wxAtoi(layerDialog.GetValue()) + 2;
        }
        Vector layerSize(layers);
        for (unsigned int layer = 1; layer < layers - 1; ++layer)
        {
            while (layerSize[layer] == 0)
            {
                wxString defaultString; defaultString << defaultValue;
                wxString text = wxT("Enter number of neurons in hidden layer ");
                text << layer << ":";
                wxTextEntryDialog layerDialog(this, text, caption,
                        defaultString);
                layerDialog.SetTextValidator(wxFILTER_DIGITS);
                if (layerDialog.ShowModal() == wxID_CANCEL) return Vector();
                layerSize[layer] = wxAtoi(layerDialog.GetValue());
            }
        }
        return layerSize;
    }

    NeuronType GameFrame::getHiddenType(const wxString& caption)
    {
        wxString choice[3];
        choice[0] = wxT("Logistic");
        choice[1] = wxT("Hyperbolic Tangent");
        choice[2] = wxT("Threshold");
        wxSingleChoiceDialog hiddenTypeDialog(this,
                wxT("Select the type of hidden neuron:"), caption, 3, choice);
        hiddenTypeDialog.SetSize(330, 220);
        hiddenTypeDialog.SetSelection(0);
        if (hiddenTypeDialog.ShowModal() == wxID_CANCEL)
            return NeuronType::Normal;
        switch (hiddenTypeDialog.GetSelection())
        {
        case 0:
            return NeuronType::Logistic;
        case 1:
            return NeuronType::HyperbolicTangent;
        case 2:
            return NeuronType::Threshold;
        default:
            return NeuronType::Normal;
        }
    }

    double GameFrame::getLearnRate(const wxString& caption,
            const double defaultValue)
    {
        double value = defaultValue;
        bool valid = false;
        while(!valid)
        {
            wxString defaultString; defaultString << defaultValue;
            wxTextEntryDialog learnRateDialog(this,
                    wxT("Enter the learn rate:"), caption,
                    defaultString);
            learnRateDialog.SetTextValidator(wxFILTER_NUMERIC);
            if (learnRateDialog.ShowModal() == wxID_CANCEL)
                return std::nan("");
            value = wxAtof(learnRateDialog.GetValue());
            if (value > 0) valid = true;
            if (!valid)
            {
                wxMessageBox(wxT("Value must be greater than 0."));
            }
        }
        return value;
    }

    double GameFrame::getDiscountRate(const wxString& caption,
            const double defaultValue)
    {
        double value = defaultValue;
        bool valid = false;
        while(!valid)
        {
            wxString defaultString; defaultString << defaultValue;
            wxTextEntryDialog discountRateDialog(this,
                    wxT("Enter the temporal discount rate:"), caption,
                    defaultString);
            discountRateDialog.SetTextValidator(wxFILTER_NUMERIC);
            if (discountRateDialog.ShowModal() == wxID_CANCEL)
                return std::nan("");
            value = wxAtof(discountRateDialog.GetValue());
            if (value >= 0 && value <= 1) valid = true;
            if (!valid)
            {
                wxMessageBox(wxT("Value must be between 0 and 1."));
            }
        }
        return value;
    }

    double GameFrame::getDefaultValue(const wxString& caption,
            const double defaultValue)
    {
        double value = defaultValue;
        wxString defaultString; defaultString << defaultValue;
        wxTextEntryDialog discountRateDialog(this,
                wxT("Enter the default value:"), caption,
                defaultString);
        discountRateDialog.SetTextValidator(wxFILTER_NUMERIC);
        if (discountRateDialog.ShowModal() == wxID_CANCEL)
            return std::nan("");
        value = wxAtof(discountRateDialog.GetValue());
        return value;
    }

    double GameFrame::getGamma(const wxString& caption,
            const double defaultValue)
    {
        double value = defaultValue;
        bool valid = false;
        while(!valid)
        {
            wxString defaultString; defaultString << defaultValue;
            wxTextEntryDialog gammaDialog(this,
                    wxT("Enter the regularisation factor (γ):"), caption,
                    defaultString);
            gammaDialog.SetTextValidator(wxFILTER_NUMERIC);
            if (gammaDialog.ShowModal() == wxID_CANCEL)
                return std::nan("");
            value = wxAtof(gammaDialog.GetValue());
            if (value >= 0) valid = true;
            if (!valid)
            {
                wxMessageBox(wxT("Value must be greater than or equal to 0."));
            }
        }
        return value;
    }

    double GameFrame::getLambda(const wxString& caption,
            const double defaultValue)
    {
        double value = defaultValue;
        bool valid = false;
        while(!valid)
        {
            wxString defaultString; defaultString << defaultValue;
            wxTextEntryDialog lambdaDialog(this,
                    wxT("Enter the weight preference (λ):"), caption,
                    defaultString);
            lambdaDialog.SetTextValidator(wxFILTER_NUMERIC);
            if (lambdaDialog.ShowModal() == wxID_CANCEL)
                return std::nan("");
            value = wxAtof(lambdaDialog.GetValue());
            if (value >= 0 && value <= 1) valid = true;
            if (!valid)
            {
                wxMessageBox(wxT("Value must be between 0 and 1."));
            }
        }
        return value;
    }
}
