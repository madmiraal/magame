// (c) 2016 - 2017: Marcel Admiraal

#include "gamecontrol.h"

namespace ma
{
    GameControl::GameControl(Game* game) :
        game(game), players(game->getPlayers()), activity(activityMutex),
        autoReset(false), slowed(false)
    {
        player = new GamePlayer*[players];
        for (unsigned int index = 0; index < players; ++index)
        {
            player[index] = new GamePlayer(index + 1);
        }
        activityMutex.Lock();
    }

    GameControl::~GameControl()
    {
        pauseLoop(true);
        activity.Signal();
        for (unsigned int index = 0; index < players; ++index)
        {
            delete player[index];
        }
        delete[] player;
        activityMutex.Unlock();
    }

    bool GameControl::processAction(const GameAction* action)
    {
        wxMutexLocker lock(gameLock);
        unsigned int playerId = game->getNextPlayer();
        if (game->processAction(action))
        {
            player[playerId-1]->nextState(game);
            // Wakes up the thread if necessary.
            activity.Signal();
            return true;
        }
        else
        {
            setStatusMessage("Invalid move!");
            return false;
        }
    }

    void GameControl::newGame()
    {
        {
            wxMutexLocker lock(gameLock);
            game->newGame();
        }
        for (unsigned int index = 0; index < players; ++index)
        {
            player[index]->newGame();
        }
        activity.Signal();
    }

    void GameControl::newMCMCLearner(const unsigned int playerId,
            const double learnRate, const double discountRate,
            const double defaultValue)
{
        if (isLoopPaused())
        {
            player[playerId - 1]->newMCMCLearner(
                    player[playerId - 1]->getStateLength(),
                    player[playerId - 1]->getPredictionLength(),
                    learnRate, discountRate, defaultValue);
            setStatusMessage("New MCMC Learner created.");
        }
        else
        {
            setStatusMessage("Pause game first.");
        }
    }

    void GameControl::newMCNetLearner(const unsigned int playerId,
            Vector layerSize, const NeuronType hiddenType,
            const double learnRate, const double discountRate,
            const double gamma)
    {
        if (isLoopPaused())
        {
            layerSize[0] = player[playerId - 1]->getStateLength();
            layerSize[layerSize.size() - 1] =
                    player[playerId - 1]->getPredictionLength();
            player[playerId - 1]->newMCNetLearner(layerSize, hiddenType,
                    learnRate, discountRate, gamma);
            setStatusMessage("New MC Net Learner created.");
        }
        else
        {
            setStatusMessage("Pause game first.");
        }
    }

    void GameControl::newTDNetLearner(const unsigned int playerId,
            Vector layerSize, const NeuronType hiddenType,
            const double learnRate, const double discountRate,
            const double gamma, const double lambda)
    {
        if (isLoopPaused())
        {
            layerSize[0] = player[playerId - 1]->getStateLength();
            layerSize[layerSize.size() - 1] =
                    player[playerId - 1]->getPredictionLength();
            player[playerId - 1]->newTDNetLearner(layerSize, hiddenType,
                    learnRate, discountRate, gamma, lambda);
            setStatusMessage("New TD Net Learner created.");
        }
        else
        {
            setStatusMessage("Pause game first.");
        }
    }

    bool GameControl::loadLearning(const unsigned int playerId, const char* filename)
    {
        if (isLoopPaused())
        {
            if (player[playerId - 1]->loadLearner(filename))
            {
                setStatusMessage("Player learning loaded.");
                return true;
            }
            else
            {
                setStatusMessage("Load failed!");
                return false;
            }
        }
        else
        {
            setStatusMessage("Pause game first.");
            return false;
        }
    }

    bool GameControl::saveLearning(const unsigned int playerId, const char* filename)
    {
        if (isLoopPaused())
        {
            if(player[playerId - 1]->saveLearner(filename))
            {
                setStatusMessage("Player learning saved.");
                return true;
            }
            else
            {
                setStatusMessage("Save failed!");
                return false;
            }
        }
        else
        {
            setStatusMessage("Pause game first.");
            return false;
        }
    }

    double GameControl::getLearningRate(const unsigned int playerId) const
    {
        return player[playerId - 1]->getLearningRate();
    }

    void GameControl::setLearningRate(const unsigned int playerId,
            const double learningRate)
    {
        player[playerId - 1]->setLearningRate(learningRate);
    }

    double GameControl::getDiscountRate(const unsigned int playerId)
    {
        return player[playerId - 1]->getDiscountRate();
    }

    void GameControl::setDiscountRate(const unsigned int playerId,
            const double discountRate)
    {
        player[playerId - 1]->setDiscountRate(discountRate);
    }

    const GamePlayer* GameControl::getGamePlayer(
            const unsigned int playerId) const
    {
        if (playerId < 1 || playerId > players) return 0;
        return player[playerId - 1];
    }

    bool GameControl::setGamePlayer(const unsigned int playerId,
            GamePlayer* gamePlayer)
    {
        if (playerId < 1 || playerId > players) return false;
        delete player[playerId - 1];
        player[playerId - 1] = gamePlayer;
        return true;
    }

    bool GameControl::isAutomated(const unsigned int playerId)
    {
        return player[playerId - 1]->isAutomated();
    }

    void GameControl::setAutomated(const unsigned int playerId, bool automate)
    {
        player[playerId - 1]->setAutomated(automate);
        setStatusMessage("");
        activity.Signal();
    }

    bool GameControl::isLearning(const unsigned int playerId)
    {
        return player[playerId - 1]->isLearning();
    }

    void GameControl::setLearning(const unsigned int playerId, bool learning)
    {
        player[playerId - 1]->setLearning(learning);
    }

    bool GameControl::isAutomatedReset()
    {
        return autoReset;
    }

    void GameControl::setAutomatedReset(bool autoReset)
    {
        this->autoReset = autoReset;
        setStatusMessage("");
        activity.Signal();
    }

    bool GameControl::isSlow()
    {
        return slowed;
    }

    void GameControl::setSlow(bool slowed)
    {
        this->slowed = slowed;
    }

    unsigned int GameControl::getPlayers()
    {
        return players;
    }

    Game* GameControl::getGameCopy()
    {
        wxMutexLocker lock(gameLock);
        return game->createCopy();
    }

    void GameControl::loopFunction()
    {
        unsigned int playerId;
        {
            wxMutexLocker lock(gameLock);
            playerId = game->getNextPlayer();
        }
        // Add a delay to allow humans to see what's happening.
        if (slowed)
        {
            if (playerId != 0)
            {
                wxMilliSleep(100);
            }
            else
            {
                // Pause longer if the game is over.
                wxMilliSleep(1000);
            }
        }
        // If game wasn't paused while waiting.
        if (!isLoopPaused())
        {
            if (playerId == 0)
            {
                for (unsigned int index = 0; index < players; index++)
                {
                    wxMutexLocker lock(gameLock);
                    player[index]->gameOver(game);
                }
                if (autoReset)
                {
                    newGame();
                }
                else
                {
                    {
                        wxMutexLocker lock(gameLock);
                        setStatusMessage(game->getStatusMessage());
                    }
                    activity.Wait();
                }
            }
            else if (player[playerId-1]->isAutomated())
            {
                wxMutexLocker lock(gameLock);
                GameAction* nextAction = player[playerId-1]->
                        getNextAction(game);
                if (game->processAction(nextAction))
                {
                    player[playerId-1]->nextState(game);
                }
                delete nextAction;
            }
            else
            {
                {
                    wxMutexLocker lock(gameLock);
                    setStatusMessage(game->getStatusMessage());
                }
                activity.Wait();
            }
        }
    }
}
