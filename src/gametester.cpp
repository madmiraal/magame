// (c) 2017 - 2019: Marcel Admiraal

#include "gametester.h"

#include "log2inc.h"

#include <cmath>

namespace ma
{
    GameTester::GameTester(Game* game, GamePlayer** player, const Str& filename,
            Vector* const* state, Vector* const* target,
            unsigned int* states, const unsigned int simulations,
            const unsigned int iterations,
            LearnerType firstLearnerType, LearnerType lastLearnerType,
            LearnerType defaultLearnerType,
            unsigned int minHiddenSize, unsigned int maxHiddenSize,
            unsigned int defaultHiddenSize,
            NeuronType firstNeuronType, NeuronType lastNeuronType,
            NeuronType defaultNeuronType,
            double minLearnRate, double maxLearnRate, double defaultLearnRate,
            double minDiscountRate, double maxDiscountRate,
            double defaultDiscountRate,
            double minGamma, double maxGamma, double defaultGamma,
            double minLambda, double maxLambda, double defaultLambda) :
        game(game), player(player), state(state), target(target),
        states(states), simulations(simulations), iterations(iterations),
        firstLearnerType(firstLearnerType), lastLearnerType(lastLearnerType),
        defaultLearnerType(defaultLearnerType),
        minHiddenSize(minHiddenSize), maxHiddenSize(maxHiddenSize),
        defaultHiddenSize(defaultHiddenSize),
        firstNeuronType(firstNeuronType), lastNeuronType(lastNeuronType),
        defaultNeuronType(defaultNeuronType),
        minLearnRate(minLearnRate), maxLearnRate(maxLearnRate),
        defaultLearnRate(defaultLearnRate),
        minDiscountRate(minDiscountRate), maxDiscountRate(maxDiscountRate),
        defaultDiscountRate(defaultDiscountRate),
        minGamma(minGamma), maxGamma(maxGamma), defaultGamma(defaultGamma),
        minLambda(minLambda), maxLambda(maxLambda), defaultLambda(defaultLambda),
        players(game->getPlayers()), points(getPoints(iterations)),
        error(simulations, points), test(new bool[7]())
    {
        file.open(filename.c_str());
        initialiseGameTestResultsFile(file, iterations);
    }

    GameTester::~GameTester()
    {
        file.close();
    }

    void GameTester::enableTest(GameTest gameTest, bool enable)
    {
        test[gameTest] = enable;
    }

    void GameTester::testLoops()
    {
        LearnerType firstLearnerType = defaultLearnerType;
        LearnerType lastLearnerType = defaultLearnerType;
        unsigned int minHiddenSize = defaultHiddenSize;
        unsigned int maxHiddenSize = defaultHiddenSize;
        NeuronType firstNeuronType = defaultNeuronType;
        NeuronType lastNeuronType = defaultNeuronType;
        double minLearnRate = defaultLearnRate;
        double maxLearnRate = defaultLearnRate;
        double minDiscountRate = defaultDiscountRate;
        double maxDiscountRate = defaultDiscountRate;
        double minGamma = defaultGamma;
        double maxGamma = defaultGamma;
        double minLambda = defaultLambda;
        double maxLambda = defaultLambda;

        if (test[LearnerTypes])
        {
            firstLearnerType = this->firstLearnerType;
            lastLearnerType = this->lastLearnerType;
        }
        if (test[HiddenSizes])
        {
            minHiddenSize = this->minHiddenSize;
            maxHiddenSize = this->maxHiddenSize;
        }
        if (test[NeuronTypes])
        {
            firstNeuronType = this->firstNeuronType;
            lastNeuronType = this->lastNeuronType;
        }
        if (test[LearnRates])
        {
            minLearnRate = this->minLearnRate;
            maxLearnRate = this->maxLearnRate;
        }
        if (test[DiscountRates])
        {
            minDiscountRate = this->minDiscountRate;
            maxDiscountRate = this->maxDiscountRate;
        }
        if (test[Gammas])
        {
            minGamma = this->minGamma;
            maxGamma = this->maxGamma;
        }
        if (test[Lambdas])
        {
            minLambda = this->minLambda;
            maxLambda = this->maxLambda;
        }

        for (LearnerType learnerType = firstLearnerType;
                learnerType <= lastLearnerType;
                learnerType = (LearnerType)(((unsigned int)learnerType)+1))
        {

        for (unsigned int hiddenSize = minHiddenSize;
                hiddenSize <= maxHiddenSize; hiddenSize += 5)
        {
            if (learnerType == LearnerType::MCMCLearner) hiddenSize = 0;

        for (NeuronType hiddenType = firstNeuronType;
                hiddenType <= lastNeuronType;
                hiddenType = (NeuronType)(((unsigned int)hiddenType)+1))
        {
            if (learnerType == LearnerType::MCMCLearner)
            {
                hiddenSize = 0;
                hiddenType = NeuronType::Normal;
            }

        for (Log2Inc learnRate = maxLearnRate; learnRate >= minLearnRate;
                --learnRate)
        {

        for (double discountRate = minDiscountRate;
                discountRate <= maxDiscountRate; discountRate += .1)
        {

        for (double gamma = minGamma; gamma <= maxGamma; gamma *= 10)
        {

        for (double lambda = minLambda; lambda <= maxLambda; lambda += .1)
        {
            if (learnerType == LearnerType::MCMCLearner ||
                learnerType == LearnerType::MCNetLearner) lambda = -1;

            std::cout << getLearnerName(learnerType) << std::endl;
            std::cout << "State length: ";
            std::cout << player[0]->getStateLength() << std::endl;
            std::cout << "Hidden size: ";
            std::cout << hiddenSize << std::endl;
            std::cout << "Hidden type: ";
            print(hiddenType); std::cout << std::endl;
            std::cout << "Learn rate: ";
            std::cout << learnRate << std::endl;
            std::cout << "Discount rate: ";
            std::cout << discountRate << std::endl;
            std::cout << "Gamma: ";
            std::cout << gamma << std::endl;
            std::cout << "Lambda: ";
            std::cout << lambda << std::endl;
            for (unsigned int simulation = 0; simulation < simulations;
                    ++simulation)
            {
                std::cout << "Simulation: ";
                std::cout << simulation+1 << " ..." << std::endl;
                for (unsigned int playerIndex = 0; playerIndex < players;
                        ++playerIndex)
                {
                    assignNewLearner(player[playerIndex], learnerType,
                            learnRate.getValue(), discountRate, hiddenSize,
                            hiddenType, gamma, lambda);
                }
                runSimulation(game, player, error[simulation], iterations,
                        state, target, states);

                std::cout << "Final Error: ";
                std::cout << error[simulation][points-1] << std::endl;
            }

            Vector mean = error.mean();
            Vector sd = error.sd();

            if (learnerType == LearnerType::MCMCLearner)
                hiddenSize = 0;
            file << getLearnerName(learnerType);
            file << "," << player[0]->getStateLength();
            file << "," << "n=" << hiddenSize;
            file << ","; print(hiddenType, file);
            file << "," << "α=" << learnRate;
            file << "," << "β=" << discountRate;
            file << "," << "γ=" << gamma;
            file << "," << "λ=" << lambda;
            file << "," << "m=" << simulations;
            for (unsigned int point = 0; point < points; ++point)
                file << "," << mean[point];
            for (unsigned int point = 0; point < points; ++point)
                file << "," << sd[point];
            file << std::endl;

            if (learnerType == LearnerType::MCMCLearner ||
                learnerType == LearnerType::MCNetLearner) lambda = 1;
            if (gamma == 0) gamma = 0.1;
        }
        }
        }
        }
            if (learnerType == LearnerType::MCMCLearner)
            {
                hiddenSize = maxHiddenSize;
                hiddenType = NeuronType::HyperbolicTangent;
            }
        }
        }
        }
    }

    void GameTester::setGame(Game* game)
    {
        this->game = game;
    }

    void GameTester::setGamePlayers(GamePlayer** player)
    {
        this->player = player;
    }

    bool GameTester::setOutputFile(const Str& filename)
    {
        file.close();
        file.open(filename.c_str());
        initialiseGameTestResultsFile(file, iterations);
        return file.good();
    }

    void GameTester::setTestStates(Vector* const* state, unsigned int* states)
    {
        this->state = state;
        if (states != 0) this->states = states;
    }

    void GameTester::setTestTargets(Vector* const* target, unsigned int* states)
    {
        this->target = target;
        if (states != 0) this->states = states;
    }

    void GameTester::setSimulations(const unsigned int simulations)
    {
        this->simulations = simulations;
        error = Matrix(simulations, points);
    }

    void GameTester::setIterations(const unsigned int iterations)
    {
        this->iterations = iterations;
        points = getPoints(iterations);
        error = Matrix(simulations, points);
    }

    void GameTester::setfirstLearnerType(const LearnerType firstLearnerType)
    {
        this->firstLearnerType = firstLearnerType;
    }

    void GameTester::setLastLearnerType(const LearnerType lastLearnerType)
    {
        this->lastLearnerType = lastLearnerType;
    }

    void GameTester::setDefaultLeanerType(const LearnerType defaultLearnerType)
    {
        this->defaultLearnerType = defaultLearnerType;
    }

    void GameTester::setMinHiddenSize(const unsigned int minHiddenSize)
    {
        this->minHiddenSize = minHiddenSize;
    }

    void GameTester::setMaxHiddenSize(const unsigned int maxHiddenSize)
    {
        this->maxHiddenSize = maxHiddenSize;
    }

    void GameTester::setDefaultHiddenSize(const unsigned int defaultHiddenSize)
    {
        this->defaultHiddenSize = defaultHiddenSize;
    }

    void GameTester::setFirstNeuronType(const NeuronType firstNeuronType)
    {
        this->firstNeuronType = firstNeuronType;
    }

    void GameTester::setLastNeuronType(const NeuronType lastNeuronType)
    {
        this->lastNeuronType = lastNeuronType;
    }

    void GameTester::setDefaultNeuronType(const NeuronType defaultNeuronType)
    {
        this->defaultNeuronType = defaultNeuronType;
    }

    void GameTester::setMinLearnRate(const double minLearnRate)
    {
        this->minLearnRate = minLearnRate;
    }

    void GameTester::setMaxLearnRate(const double maxLearnRate)
    {
        this->maxLearnRate = maxLearnRate;
    }

    void GameTester::setDefaultLearnRate(const double defaultLearnRate)
    {
        this->defaultLearnRate = defaultLearnRate;
    }

    void GameTester::setMinDiscountRate(const double minDiscountRate)
    {
        this->minDiscountRate = minDiscountRate;
    }

    void GameTester::setMaxDiscountRate(const double maxDiscountRate)
    {
        this->maxDiscountRate = maxDiscountRate;
    }

    void GameTester::setDefaultDiscountRate(const double defaultDiscountRate)
    {
        this->defaultDiscountRate = defaultDiscountRate;
    }

    void GameTester::setMinGamma(const double minGamma)
    {
        this->minGamma = minGamma;
    }

    void GameTester::setMaxGamma(const double maxGamma)
    {
        this->maxGamma = maxGamma;
    }

    void GameTester::setDefaultGamma(const double defaultGamma)
    {
        this->defaultGamma = defaultGamma;
    }

    void GameTester::setMinLambda(const double minLambda)
    {
        this->minLambda = minLambda;
    }

    void GameTester::setMaxLambda(const double maxLambda)
    {
        this->maxLambda = maxLambda;
    }

    void GameTester::setDefaultLambda(const double defaultLambda)
    {
        this->defaultLambda = defaultLambda;
    }

    Str getLearnerName(const LearnerType type)
    {
        Str name;
        switch (type)
        {
        case LearnerType::MCMCLearner:
            name = "MCMC Learner";
            break;
        case LearnerType::MCNetLearner:
            name = "MC Net Learner";
            break;
        case LearnerType::TDNetLearner:
            name = "TD Net Learner";
            break;
        }
        return name;
    }

    void assignNewLearner(GamePlayer* player, const LearnerType learnerType,
            const double learnRate, const double discountRate,
            unsigned int hiddenSize, const NeuronType hiddenType,
            const double gamma, const double lambda)
    {
        Vector layerSize(3);
        layerSize[0] = player->getStateLength();
        layerSize[1] = hiddenSize;
        layerSize[2] = player->getPredictionLength();
        switch (learnerType)
        {
        case LearnerType::MCMCLearner:
            player->newMCMCLearner(player->getStateLength(),
                    player->getPredictionLength(), learnRate, discountRate);
            break;
        case LearnerType::MCNetLearner:
            player->newMCNetLearner(layerSize, hiddenType, learnRate,
                    discountRate, gamma);
            break;
        case LearnerType::TDNetLearner:
            player->newTDNetLearner(layerSize, hiddenType, learnRate,
                    discountRate, gamma, lambda);
        }
    }

    void teachPlayers(Game* game, GamePlayer** player,
            const unsigned int iterations)
    {
        for (unsigned int iteration = 0; iteration < iterations; ++iteration)
        {
            unsigned int nextPlayer = game->getNextPlayer();
            while (nextPlayer != 0)
            {
                GameAction* nextAction =
                        player[nextPlayer-1]->getNextAction(game);
                if (game->processAction(nextAction))
                {
                    player[nextPlayer-1]->nextState(game);
                }
                else
                {
                    throw "Invalid action.";
                }
                delete nextAction;
                nextPlayer = game->getNextPlayer();
            }
            for (unsigned int playerIndex = 0; playerIndex < game->getPlayers();
                    ++playerIndex)
            {
                player[playerIndex]->gameOver(game);
            }
            game->newGame();
        }
    }

    double testPlayers(GamePlayer** player, const unsigned int players,
            const Vector* const* state, const Vector* const* target,
            const unsigned int* states)
    {
        double sum = 0;
        unsigned int values = 0;
        for (unsigned int playerIndex = 0; playerIndex < players; ++playerIndex)
        {
            if (states[playerIndex] > 0)
            {
                values += states[playerIndex] * target[playerIndex][0].size();
            }
        }
        for (unsigned int playerIndex = 0; playerIndex < players; ++playerIndex)
        {
            for (unsigned int stateIndex = 0;
                    stateIndex < states[playerIndex]; ++stateIndex)
            {
                Vector prediction = player[playerIndex]->getPrediction(
                        state[playerIndex][stateIndex]);
                Vector error = prediction - target[playerIndex][stateIndex];
                sum += error.elementSquaredSum();
            }
        }
        return std::sqrt(sum / values);
    }

    void runSimulation(Game* game, GamePlayer** player, double* error,
            const unsigned int iterations,
            const Vector* const* state, const Vector* const* target,
            const unsigned int* states)
    {
        unsigned int players = game->getPlayers();
        unsigned int point = 0;
        unsigned int iterationsDone = 0;
        error[point] = testPlayers(player, players, state, target, states);

        for (Log2Inc totalIterations; totalIterations <= iterations;
                ++totalIterations)
        {
            teachPlayers(game, player,
                    totalIterations.getValue() - iterationsDone);
            error[++point] = testPlayers(player, players,
                    state, target, states);
            iterationsDone = totalIterations.getValue();
        }
    }

    unsigned int getPoints(const unsigned int iterations)
    {
        unsigned int points = 1;
        for (Log2Inc i; i <= iterations; ++i) ++points;
        return points;
    }

    void initialiseGameTestResultsFile(std::ofstream& file,
            const unsigned int iterations)
    {
        file << "Learner Type";
        file << ",State Length";
        file << ",Hidden Neurons";
        file << ",Hidden Type";
        file << ",Learning Rate";
        file << ",Discount Rate";
        file << ",Gamma";
        file << ",Lambda";
        file << ",Simulations";
        file << ",0";
        for (Log2Inc i; i <= iterations; ++i) file << "," << i;
        file << ",0SD";
        for (Log2Inc i; i <= iterations; ++i) file << "," << i << "SD";
        file << std::endl;
    }
}
