// (c) 2017: Marcel Admiraal

#include "gameaction.h"

#include <algorithm>

namespace ma
{
    GameAction::GameAction() :
        GameAction(0, 0)
    {
    }

    GameAction::GameAction(const unsigned int source,
            const unsigned int destination) :
        source(source), destination(destination)
    {
    }

    GameAction::GameAction(const GameAction& original) :
        source(original.source), destination(original.destination)
    {
    }

    void swap(GameAction& first, GameAction& second)
    {
        std::swap(first.source, second.source);
        std::swap(first.destination, second.destination);
    }

    GameAction& GameAction::operator=(GameAction source)
    {
        swap(*this, source);
        return *this;
    }

    GameAction::~GameAction()
    {
    }

    GameAction* GameAction::createCopy() const
    {
        return new GameAction(*this);
    }

    unsigned int GameAction::getSource() const
    {
        return source;
    }

    unsigned int GameAction::getDestination() const
    {
        return destination;
    }
}
