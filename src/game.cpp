// (c) 2016 - 2017: Marcel Admiraal

#include "game.h"

#include <algorithm>

namespace ma
{
    Game::Game() :
        players(1), player(1), winner(0), statusMessage("")
    {
    }

    Game::Game(const Game& source) :
        players(source.players), player(source.player), winner(source.winner),
        statusMessage(source.statusMessage)
    {
    }

    void swap(Game& first, Game& second)
    {
        std::swap(first.players, second.players);
        std::swap(first.player, second.player);
        std::swap(first.winner, second.winner);
        std::swap(first.statusMessage, second.statusMessage);
    }

    Game& Game::operator=(Game source)
    {
        swap(*this, source);
        return *this;
    }

    Game::~Game()
    {
    }

    Game* Game::createCopy() const
    {
        std::cout << "Creating copy of base game." << std::endl;
        return new Game(*this);
    }

    bool Game::processAction(const GameAction* action)
    {
        std::cout << "Processing action of base game." << std::endl;
        player = 0;
        winner = 1;
        return true;
    }


    List<GameAction*> Game::getPossibleActions() const
    {
        std::cout << "Returning possible actions of base game." << std::endl;
        List<GameAction*> possibleActions;
        possibleActions.append(new GameAction());
        return possibleActions;
    }

    void Game::newGame()
    {
        std::cout << "Resetting base game." << std::endl;
        player = 1;
        winner = 0;
    }

    unsigned int Game::getPlayers() const
    {
        return players;
    }

    unsigned int Game::getNextPlayer() const
    {
        return player;
    }

    unsigned int Game::getWinner() const
    {
        return winner;
    }

    Str Game::getStatusMessage() const
    {
        return statusMessage;
    }

}
