// (c) 2017: Marcel Admiraal

#include "gamepolicy.h"

#include <cfloat>

namespace ma
{
    GamePolicy::~GamePolicy()
    {
    }

    RandomGamePolicy::~RandomGamePolicy()
    {
    }

    GamePolicy* RandomGamePolicy::createCopy() const
    {
        return new RandomGamePolicy();
    }

    GameAction* RandomGamePolicy::selectAction(const Game* game,
            GamePlayer* player, List<GameAction*>& possibleActions)
    {
        unsigned int index = rand() % possibleActions.getSize();
        return possibleActions.get(index);
    }

    GreedyGamePolicy::~GreedyGamePolicy()
    {
    }

    GamePolicy* GreedyGamePolicy::createCopy() const
    {
        return new GreedyGamePolicy();
    }

    GameAction* GreedyGamePolicy::selectAction(const Game* game,
            GamePlayer* player, List<GameAction*>& possibleActions)
    {
        double highestValue = -DBL_MAX;
        GameAction* bestAction = 0;
        for (Iterator<GameAction*> iter = possibleActions.first(); iter.valid();
                ++iter)
        {
            GameAction* thisAction = *iter;
            Game* gameCopy = game->createCopy();
            if (gameCopy->processAction(thisAction))
            {
                double thisValue = player->getGameValue(gameCopy);
                if (thisValue > highestValue)
                {
                    highestValue = thisValue;
                    bestAction = thisAction;
                }
            }
            delete gameCopy;
        }
        return bestAction;
    }

    ProbabilityGamePolicy::~ProbabilityGamePolicy()
    {
    }

    GamePolicy* ProbabilityGamePolicy::createCopy() const
    {
        return new ProbabilityGamePolicy();
    }

    GameAction* ProbabilityGamePolicy::selectAction(const Game* game,
            GamePlayer* player, List<GameAction*>& possibleActions)
    {
        double valueTotal = 0;
        for (Iterator<GameAction*> iter = possibleActions.first();
                iter.valid(); ++iter)
        {
            GameAction* thisAction = *iter;
            Game* gameCopy = game->createCopy();
            if (gameCopy->processAction(thisAction))
            {
                valueTotal += player->getGameValue(gameCopy);
            }
            delete gameCopy;
        }

        double limit = 1.0 * rand() / RAND_MAX * valueTotal;
        valueTotal = 0;
        for (Iterator<GameAction*> iter = possibleActions.first();
                iter.valid(); ++iter)
        {
            GameAction* thisAction = *iter;
            Game* gameCopy = game->createCopy();
            if (gameCopy->processAction(thisAction))
            {
                valueTotal += player->getGameValue(gameCopy);
                if (valueTotal >= limit) return thisAction;
            }
            delete gameCopy;
        }
        throw "Policy error.";
    }

    ESplitGamePolicy::ESplitGamePolicy(const GamePolicy& first,
            const GamePolicy& second, const double e) :
        first(first.createCopy()), second(second.createCopy()), e(e)
    {
    }

    ESplitGamePolicy::~ESplitGamePolicy()
    {
        delete first;
        delete second;
    }

    GamePolicy* ESplitGamePolicy::createCopy() const
    {
        return new ESplitGamePolicy(*first, *second, e);
    }

    GameAction* ESplitGamePolicy::selectAction(const Game* game,
            GamePlayer* player, List<GameAction*>& possibleActions)
    {
        if ((1.0 * rand() / RAND_MAX) > e)
        {
            return first->selectAction(game, player, possibleActions);
        }
        else
        {
            return second->selectAction(game, player, possibleActions);
        }
    }

    EGreedyGamePolicy::EGreedyGamePolicy(const double e) :
        ESplitGamePolicy(GreedyGamePolicy(), RandomGamePolicy(), e)
    {
    }

    EGreedyGamePolicy::~EGreedyGamePolicy()
    {
    }

    GamePolicy* EGreedyGamePolicy::createCopy() const
    {
        return new EGreedyGamePolicy(e);
    }

    EGreedyProbabilityGamePolicy::EGreedyProbabilityGamePolicy(const double e) :
        ESplitGamePolicy(GreedyGamePolicy(), RandomGamePolicy(), e)
    {
    }

    EGreedyProbabilityGamePolicy::~EGreedyProbabilityGamePolicy()
    {
    }

    GamePolicy* EGreedyProbabilityGamePolicy::createCopy() const
    {
        return new EGreedyProbabilityGamePolicy(e);
    }
}
