// Generic cross-platform game control frame.

// (c) 2016 - 2017: Marcel Admiraal

#ifndef GAMEFRAME_H
#define GAMEFRAME_H

#include "controlframe.h"
#include "game.h"
#include "gamecontrol.h"

namespace ma
{
    class GameFrame : public ControlFrame
    {
    public:
        /**
         * Game constructor.
         *
         * @param game      The game.
         * @param title     The text displayed at the top of the window.
         * @param position  A wxPoint identifying the location of the window.
         * @param size      A wxSize identifying the size of the window.
         * @param syle      The combination of wxstyle codes.
         * @param parent    The parent of the window, if any.
         * @param id        The window's id.
         * @param name      A name for the window.
         */
        GameFrame(
            Game* game,
            wxWindow* parent = 0,
            const wxWindowID id = wxID_ANY,
            const Str& title = Str(),
            const wxPoint& position = wxDefaultPosition,
            const wxSize& size = wxDefaultSize,
            const long style = wxDEFAULT_FRAME_STYLE,
            const wxString& name = wxFrameNameStr);

        /**
         * Destructor.
         */
        virtual ~GameFrame();

        /**
         * Prompt user to load saved learning.
         */
        void loadLearners();

    protected:
        GameControl* gameControl;

    private:
        // Get the desired player id.
        unsigned int getPlayerId(
                const wxString& caption = wxT("Select Player"));
        // On selecting new learner.
        void newLearner(wxCommandEvent& event);
        // Create a new learner for the specified player.
        void newLearner(unsigned int playerId);
        // On selecting Load Learning.
        void loadLearner(wxCommandEvent& event);
        // Load specified player's learning.
        void loadLearner(unsigned int playerId);
        // Prompt to save players' learning.
        void saveLearners();
        // On selecting Save Learning.
        void saveLearner(wxCommandEvent& event);
        // Save specified player's learning.
        void saveLearner(unsigned int playerId);
        // On selecting Set Learning Rate.
        void setLearningRate(wxCommandEvent& event);
        // Set the specified player's learning rate.
        void setLearningRate(unsigned int playerId);
        // On selecting Set Discount Rate.
        void setDiscountRate(wxCommandEvent& event);
        // Set the specified player's discount rate.
        void setDiscountRate(unsigned int playerId);
        // On selecting/deselecting automate player.
        void automatePlayer(wxCommandEvent& event);
        // On selecting/deselecting automate reset.
        void automateRestart(wxCommandEvent& event);
        // On selecting/deselecting learn.
        void setLearning(wxCommandEvent& event);
        // On selecting/deselecting slow.
        void setSlow(wxCommandEvent& event);
        // Manual reset event.
        void reset(wxCommandEvent& event);
        // Switch pause event.
        void switchPause(wxCommandEvent& event);
        // Pause function.
        void switchPause();
        // On close event.
        void close(wxCloseEvent& event);
        // Quit event.
        void quit(wxCommandEvent& event);

        // Get the sizes of the hidden layers required.
        Vector getHiddenLayerSizes(const wxString& caption,
                const double defaultValue);
        // Get the the hidden neuron type required.
        NeuronType getHiddenType(const wxString& caption);
        // Get the learn rate required.
        double getLearnRate(const wxString& caption,
                const double defaultValue);
        // Get the discount rate required.
        double getDiscountRate(const wxString& caption,
                const double defaultValue);
        // Get the default value required.
        double getDefaultValue(const wxString& caption,
                const double defaultValue);
        // Get the gamma required.
        double getGamma(const wxString& caption,
                const double defaultValue);
        // Get the lambda required.
        double getLambda(const wxString& caption,
                const double defaultValue);

        wxToolBarToolBase* pauseTool, * continueTool;
        int autoId, learnId, pauseId, continueId;
    };
}

#endif // GAMEFRAME_H
