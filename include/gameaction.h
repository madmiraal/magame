// Generic cross-platform base class for creating actions that are processed
// by Game.

// (c) 2016 - 2017: Marcel Admiraal

#ifndef GAMEACTION_H
#define GAMEACTION_H

namespace ma
{
    class GameAction
    {
    public:
        /**
         * Default constructor.
         */
        GameAction();

        /**
         * Source and destination constructor.
         */
        GameAction(const unsigned int source, const unsigned int destination);

        /**
         * Copy constructor.
         */
        GameAction(const GameAction& source);

        /**
         * Swap function.
         *
         * @param first  The first action to swap with.
         * @param second The second action to swap with.
         */
        friend void swap(GameAction& first, GameAction& second);

        /**
         * Assignment operator.
         * Returns a copy of the action.
         *
         * @param source The action to copy.
         * @return       A copy of the action.
         */
        GameAction& operator=(GameAction source);

        /**
         * Default destructor.
         */
        virtual ~GameAction();

        /**
         * Returns a pointer to a copy of this game action.
         * The caller takes ownership of the action and must delete it.
         *
         * @return A pointer to a copy of this action.
         */
        virtual GameAction* createCopy() const;

        /**
         * Returns the source of a move action.
         *
         * @return The source of a move action.
         */
        unsigned int getSource() const;

        /**
         * Returns the destination of a move action.
         *
         * @return The destination of a move action.
         */
        unsigned int getDestination() const;

    protected:
        unsigned int source, destination;
    };
}

#endif // GAMEACTION_H
