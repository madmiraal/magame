// Generic cross-platform game player.
//
// Override:
// double getGameValue(const Game* game) const;
// Vector getGameState(const Game* game) const;
// Vector getFinalReward(const Game* game) const;

// (c) 2016 - 2017: Marcel Admiraal

#ifndef GAMEPLAYER_H
#define GAMEPLAYER_H

#include "predictor.h"
#include "game.h"
#include "gameaction.h"

namespace ma
{
    class GamePolicy;

    class GamePlayer : public Predictor
    {
    public:
        /**
         * Constructor.
         *
         * @param playerId  The id of this player.
         */
        GamePlayer(const unsigned int playerId);

        /**
         * Default destructor.
         */
        virtual ~GamePlayer();

        /**
         * Return the player's next action.
         *
         * @param game  The current game.
         */
        GameAction* getNextAction(const Game* game);

        /**
         * Inform the player of the next game state.
         *
         * @param game  The current game.
         */
        void nextState(const Game* game);

        /**
         * Inform the player of the game end.
         *
         * @param game  The end game.
         */
        void gameOver(const Game* game);

        /**
         * Inform the player a new game has started.
         */
        void newGame();

        /**
         * Returns the player's id.
         *
         * @return The player's id.
         */
        unsigned int getPlayerId() const;

        /**
         * Sets whether or not the player is automated.
         *
         * @param automated Whether or not the player is automated.
         */
        void setAutomated(bool automated = true);

        /**
         * Returns whether or not the player is automated.
         *
         * @return Whether or not the player is automated.
         */
        bool isAutomated() const;

        /**
         * Sets the learning policy.
         *
         * @param learningPolicy    The learning policy.
         */
        void setLearningPolicy(const GamePolicy& learningPolicy);

        /**
         * Sets the not learning policy.
         *
         * @param learningPolicy    The learning policy.
         */
        void setNotLearningPolicy(const GamePolicy& learningPolicy);

        /**
         * Return the player's valuation of the game.
         * Converts the learner's output vector into a double.
         *
         * @param game  The game to evaluate.
         * @return      The player's valuation of the game.
         */
        virtual double getGameValue(const Game* game);

    protected:
        /**
         * Returns the game's state vector of state length.
         *
         * @param game  The game.
         * @return      The game's state vector of state length.
         */
        virtual Vector getGameState(const Game* game) const;

        /**
         * Returns the game's reward.
         *
         * @param game  The game.
         * @return      The game's reward vector of output length.
         */
        virtual Vector getResult(const Game* game) const;

    private:
        // Stores the player number for this player.
        const unsigned int playerId;
        // Stores whether or not this player is automated.
        bool automated;
        // Learning policy.
        GamePolicy* learningPolicy;
        // Not learning policy.
        GamePolicy* notLearningPolicy;
    };
}

#endif // GAMEPLAYER_H
