// Generic cross-platform base class for creating automated multi-player games.

// (c) 2016 - 2017: Marcel Admiraal

#ifndef GAME_H
#define GAME_H

#include "gameaction.h"
#include "list.h"
#include "str.h"

namespace ma
{
    class Game
    {
    public:
        /**
         * Default constructor.
         */
        Game();

        /**
         * Copy constructor.
         */
        Game(const Game& source);

        /**
         * Swap function.
         *
         * @param first  The first game to swap with.
         * @param second The second game to swap with.
         */
        friend void swap(Game& first, Game& second);

        /**
         * Assignment operator.
         * Returns a copy of the game.
         *
         * @param source The game to copy.
         * @return       A copy of the game.
         */
        Game& operator=(Game source);

        /**
         * Default destructor.
         */
        virtual ~Game();

        /**
         * Returns a pointer to a copy of this game.
         * The caller takes ownership of the game and must delete it.
         *
         * @return A pointer to a copy of this game.
         */
        virtual Game* createCopy() const;

        /**
         * Attempts to process an action.
         * Returns whether or not the action was valid.
         *
         * @param action The action.
         * @return       Whether or not the action was valid.
         */
        virtual bool processAction(const GameAction* action);

        /**
         * Returns a list of possible actions.
         * The caller must take ownership of the pointers and delete them.
         *
         * @return A list of possible actions.
         */
        virtual List<GameAction*> getPossibleActions() const;

        /**
         * Resets the game back to the starting position.
         */
        virtual void newGame();

        /**
         * Returns the number of players.
         *
         * @return The number of players.
         */
        unsigned int getPlayers() const;

        /**
         * Returns the player to play next.
         * Returns zero if the game is over.
         *
         * @return The player to play next.
         */
        unsigned int getNextPlayer() const;

        /**
         * Returns the player that has won, or zero if the game is a draw.
         *
         * @return The player that has won, or zero if the game is a draw.
         */
        unsigned int getWinner() const;

        /**
         * Returns a message indicating the status of the game.
         *
         * @return A message indicating the status of the game.
         */
        Str getStatusMessage() const;

    protected:
        // The number of players.
        unsigned int players;
        // Player to play: 0 = Game over.
        unsigned int player;
        // Winner: 0 = Game in play.
        unsigned int winner;
        // Store status message.
        Str statusMessage;
    };
}

#endif // GAME_H
