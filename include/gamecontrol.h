// Generic cross-platform game controller.

// (c) 2016 - 2017: Marcel Admiraal

#ifndef GAMECONTROL_H
#define GAMECONTROL_H

#include "game.h"
#include "gameplayer.h"
#include "control.h"
#include "vector.h"

namespace ma
{
    class GamePlayer;

    class GameControl : public Control
    {
    public:
        /**
         * Constructor.
         */
        GameControl(Game* game);

        /**
         * Destructor.
         */
        virtual ~GameControl();

        /**
         * Processes an action. Returns whether or not the action was valid.
         *
         * @param action    The action.
         * @return          Whether or not the action was valid.
         */
        bool processAction(const GameAction* action);

        /**
         * Starts a new game.
         */
        void newGame();

        /**
         * Create a new MCMC Learner for a player.
         *
         * @param playerId  The id of the player.
         * @param learnRate         The learning rate. Default: 0.1.
         * @param discountRate      The reward discount rate. Default: 1.0.
         * @param defaultValue      The default vector values. Default: 0.5.
         */
        void newMCMCLearner(const unsigned int playerId,
                const double learnRate = 0.1,
                const double discountRate = 1.0,
                const double defaultValue = 0.5);

        /**
         * Create a new Net Learner for a player.
         *
         * @param playerId  The id of the player.
         * @param layerSize     The vector containing the layer sizes.
         * @param hiddenType    The neuron type for the hidden layers.
         *                      Default: Logistic
         * @param learnRate     The learning rate. Default: 0.1.
         * @param discountRate  The reward discount rate. Default: 1.0.
         * @param gamma         The regularisation factor. Default: 0.001.
         */
        void newMCNetLearner(const unsigned int playerId, Vector layerSize,
                const NeuronType hiddenType = NeuronType::Logistic,
                const double learnRate = 0.1, const double discountRate = 1.0,
                const double gamma = 0.001);

        /**
         * Create a new TD Learner for a player.
         *
         * @param playerId  The id of the player.
         * @param layerSize     The vector containing the layer sizes.
         * @param hiddenType    The neuron type for the hidden layers.
         *                      Default: Logistic
         * @param learnRate     The learning rate. Default: 0.1.
         * @param discountRate  The reward discount rate. Default: 1.0.
         * @param gamma         The regularisation factor. Default: 0.001.
         * @param lambda        The prediction discount rate. Default: 0.7.
         */
        void newTDNetLearner(const unsigned int playerId, Vector layerSize,
                const NeuronType hiddenType = NeuronType::Logistic,
                const double learnRate = 0.1, const double discountRate = 1.0,
                const double gamma = 0.001, const double lambda = 0.7);

        /**
         * Load previously saved learning.
         *
         * @param playerId  The id of the player.
         * @param filename  The name of the file to load from.
         * @return          Whether or not the load was successful.
         */
        bool loadLearning(const unsigned int playerId, const char* filename);

        /**
         * Save the player's current learning.
         *
         * @param playerId  The id of the player.
         * @param filename  The name of the file to save to.
         * @return          Whether or not the load was successful.
         */
        bool saveLearning(const unsigned int playerId, const char* filename);

        /**
         * Returns the specified player's learning rate.
         *
         * @return The specified player's learning rate.
         */
        double getLearningRate(const unsigned int playerId) const;

        /**
         * Sets the specified player's learning rate.
         *
         * @param learningRate  The specified player's new learning rate.
         */
        void setLearningRate(const unsigned int playerId,
                const double learningRate);

        /**
         * Returns the specified player's discount rate.
         *
         * @return The specified player's discount rate.
         */
        double getDiscountRate(const unsigned int playerId);

        /**
         * Sets the specified player's discount rate.
         *
         * @param discountRate  The specified player's new discount rate.
         */
        void setDiscountRate(const unsigned int playerId,
                const double discountRate);

        /**
         * Returns the required player.
         *
         * @param playerId  The id of the player.
         * @return          The required player.
         */
        const GamePlayer* getGamePlayer(const unsigned int playerId) const;

        /**
         * Sets the required player.
         * Returns whether or not the assignment was successful. If successful,
         * the control will take ownership of the player and delete it.
         *
         * @param playerId  The id of the player.
         * @return          Whether or not the assignment was successful.
         */
        bool setGamePlayer(const unsigned int playerId, GamePlayer* gamePlayer);

        /**
         * Returns whether or not the player is automated.
         *
         * @return Whether or not the player is automated.
         */
        bool isAutomated(const unsigned int playerId);

        /**
         * Sets whether or not the player is automated.
         *
         * @param playerId  The id of the player.
         * @param automate  Whether or not to automate the player.
         */
        void setAutomated(const unsigned int playerId, bool automate = true);

        /**
         * Returns whether or not the player is learning.
         *
         * @return Whether or not the player is learning.
         */
        bool isLearning(const unsigned int playerId);

        /**
         * Sets whether or not the player is learning.
         *
         * @param playerId  The id of the player.
         * @param automate  Whether or not to the player learns.
         */
        void setLearning(const unsigned int playerId, bool learning = true);

        /**
         * Returns whether or not the game reset is automated.
         *
         * @return Whether or not the game reset is automated.
         */
        bool isAutomatedReset();

        /**
         * Sets whether or not to automate the game reset.
         *
         * @param automate  Whether or not to automate the game reset.
         */
        void setAutomatedReset(bool autoReset = true);

        /**
         * Returns whether or not the game is slowed for human viewing.
         *
         * @return Whether or not the game is slowed for human viewing.
         */
        bool isSlow();

        /**
         * Slows the automation, so a human can watch.
         *
         * @param automate  Whether or not to slow the output.
         */
        void setSlow(bool slowed = true);

        /**
         * Returns the number of players.
         *
         * @return The number of players.
         */
        unsigned int getPlayers();

        /**
         * Returns a pointer to a copy of the game.
         * The caller takes ownership of the game and must delete it.
         *
         * @return A pointer to a copy of the game.
         */
        Game* getGameCopy();

    private:
        // Overrides the loop function.
        virtual void loopFunction();

        // Pointer to the game
        Game* game;
        // Stores the number of players.
        unsigned int players;
        // Array of player pointers.
        GamePlayer** player;
        // Mutexes
        wxMutex gameLock, signalLock, activityMutex;
        // Wait for non-automated activity.
        wxCondition activity;
        // Controls.
        bool autoReset, slowed;
    };
}

#endif // GAMECONTROL_H
