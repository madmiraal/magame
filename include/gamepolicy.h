// Generic cross-platform base class for creating game policies.
// Includes sample policies:
// - Random policy: Returns an action at random.
// - Greedy policy: Returns the action with the highest value.
// - Probability policy: Returns an action proportional to its value.
// - e-Split policy: Uses one of two policies a proportional amount of the time.
// - e-Greedy policy: Uses the greedy policy most of the time and random
//      otherwise.
// - e-GreedyProbability policy: Uses a greedy policy most of the time and a
//      proportional policy the remaninder of the time.

// (c) 2017: Marcel Admiraal

#ifndef GAMEPOLICY_H
#define GAMEPOLICY_H

#include "game.h"
#include "gameplayer.h"
#include "list.h"

namespace ma
{
    class GamePolicy
    {
    public:
        /**
         * Default destructor.
         */
        virtual ~GamePolicy();

        /**
         * Returns a pointer to a copy of this game policy.
         * The caller takes ownership of the game policy and must delete it.
         *
         * @return A pointer to a copy of this game policy.
         */
        virtual GamePolicy* createCopy() const = 0;

        /**
         * Returns the policy selected action.
         *
         * @param game              Pointer to the current game.
         * @param player            Pointer to the player.
         * @param possibleActions   List of possible actions.
         */
        virtual GameAction* selectAction(const Game* game, GamePlayer* player,
            List<GameAction*>& possibleActions) = 0;
    };

    class RandomGamePolicy : public GamePolicy
    {
    public:
        /**
         * Default destructor.
         */
        virtual ~RandomGamePolicy();

        /**
         * Returns a pointer to a copy of this game policy.
         * The caller takes ownership of the game policy and must delete it.
         *
         * @return A pointer to a copy of this game policy.
         */
        virtual GamePolicy* createCopy() const;

        /**
         * Returns the policy selected action.
         *
         * @param game              Pointer to the current game.
         * @param player            Pointer to the player.
         * @param possibleActions   List of possible actions.
         */
        virtual GameAction* selectAction(const Game* game, GamePlayer* player,
            List<GameAction*>& possibleActions);
    };

    class GreedyGamePolicy : public GamePolicy
    {
    public:
        /**
         * Default destructor.
         */
        virtual ~GreedyGamePolicy();

        /**
         * Returns a pointer to a copy of this game policy.
         * The caller takes ownership of the game policy and must delete it.
         *
         * @return A pointer to a copy of this game policy.
         */
        virtual GamePolicy* createCopy() const;

        /**
         * Returns the policy selected action.
         *
         * @param game              Pointer to the current game.
         * @param player            Pointer to the player.
         * @param possibleActions   List of possible actions.
         */
        virtual GameAction* selectAction(const Game* game, GamePlayer* player,
            List<GameAction*>& possibleActions);
    };

    class ProbabilityGamePolicy : public GamePolicy
    {
    public:
        /**
         * Default destructor.
         */
        virtual ~ProbabilityGamePolicy();

        /**
         * Returns a pointer to a copy of this game policy.
         * The caller takes ownership of the game policy and must delete it.
         *
         * @return A pointer to a copy of this game policy.
         */
        virtual GamePolicy* createCopy() const;

        /**
         * Returns the policy selected action.
         *
         * @param game              Pointer to the current game.
         * @param player            Pointer to the player.
         * @param possibleActions   List of possible actions.
         */
        virtual GameAction* selectAction(const Game* game, GamePlayer* player,
            List<GameAction*>& possibleActions);
    };

    class ESplitGamePolicy : public GamePolicy
    {
    public:
        /**
         * Constructor.
         *
         * @param first     The first policy to use.
         * @param second    The second policy to use.
         * @param e         The fraction of times the second policy is used.
         */
        ESplitGamePolicy(const GamePolicy& first, const GamePolicy& second,
                const double e = 0.5);

        /**
         * Default destructor.
         */
        virtual ~ESplitGamePolicy();

        /**
         * Returns a pointer to a copy of this game policy.
         * The caller takes ownership of the game policy and must delete it.
         *
         * @return A pointer to a copy of this game policy.
         */
        virtual GamePolicy* createCopy() const;

        /**
         * Returns the policy selected action.
         *
         * @param game              Pointer to the current game.
         * @param player            Pointer to the player.
         * @param possibleActions   List of possible actions.
         */
        virtual GameAction* selectAction(const Game* game, GamePlayer* player,
            List<GameAction*>& possibleActions);

    protected:
        GamePolicy* first;
        GamePolicy* second;
        double e;
    };

    class EGreedyGamePolicy : public ESplitGamePolicy
    {
    public:
        /**
         * Constructor.
         *
         * @param e The fraction of times a random action is selected.
         */
        EGreedyGamePolicy(const double e = 0.1);

        /**
         * Default destructor.
         */
        virtual ~EGreedyGamePolicy();

        /**
         * Returns a pointer to a copy of this game policy.
         * The caller takes ownership of the game policy and must delete it.
         *
         * @return A pointer to a copy of this game policy.
         */
        virtual GamePolicy* createCopy() const;
    };

    class EGreedyProbabilityGamePolicy : public ESplitGamePolicy
    {
    public:
        /**
         * Constructor.
         *
         * @param e The fraction of times a probable action is selected.
         */
        EGreedyProbabilityGamePolicy(const double e = 0.1);

        /**
         * Default destructor.
         */
        virtual ~EGreedyProbabilityGamePolicy();

        /**
         * Returns a pointer to a copy of this game policy.
         * The caller takes ownership of the game policy and must delete it.
         *
         * @return A pointer to a copy of this game policy.
         */
        virtual GamePolicy* createCopy() const;
    };
}

#endif // GAMEPOLICY_H
