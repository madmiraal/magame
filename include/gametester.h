// Generic cross-platform class for testing a game and its players.

// (c) 2017 - 2019: Marcel Admiraal

#ifndef GAMETESTER_H
#define GAMETESTER_H

#include "game.h"
#include "gameplayer.h"
#include "vector.h"
#include "matrix.h"

#include <fstream>

namespace ma
{
    /**
     * The types of game test.
     */
    enum GameTest
    {
        LearnerTypes,
        HiddenSizes,
        NeuronTypes,
        LearnRates,
        DiscountRates,
        Gammas,
        Lambdas
    };

    class GameTester
    {
    public:
        /**
         * Constructor.
         *
         * @param game                  The game to test.
         * @param player                The array of player pointers to use.
         * @param state                 The arrays of players' state vectors to
         *                              test with.
         * @param target                The arrays of players' target vectors to
         *                              test with.
         * @param states                The sizes of each players' test arrays.
         * @param simulations           The number of simulations for each test.
         *                              Default: 10
         * @param iterations            The number of games for each simulation.
         *                              Default: 1000
         * @param firstLearnerType      The first learner type.
         *                              Default: LearnerType::MCMC
         * @param lastLearnerType       The last learner type.
         *                              Default: LearnerType::TDNet
         * @param defaultLearnerType    The default learner type.
         *                              Default: LearnerType::TDNet
         * @param minHiddenSize         The minimum hidden layer size.
         *                              Default: 5
         * @param maxHiddenSize         The maximum hidden layer size.
         *                              Default: 50
         * @param defaultHiddenSize     The default hidden layer size.
         *                              Default: 25
         * @param firstNeuronType       The first hidden neuron type.
         *                              Default: NeuronType::Threshold
         * @param lastNeuronType        The last hidden neuron type.
         *                              Default: NeuronType::HyperbolicTangent
         * @param defaultNeuronType     The default hidden neuron type.
         *                              Default: NeuronType::Logistic
         * @param minLearnRate          The minimum learning rate.
         *                              Default: 0.001
         * @param maxLearnRate          The maximum learning rate.
         *                              Default: 1
         * @param defaultLearnRate      The default learning rate.
         *                              Default: 0.25
         * @param minDiscountRate       The minimum discount rate.
         *                              Default: 0
         * @param maxDiscountRate       The maximum discount rate.
         *                              Default: 1
         * @param defaultDiscountRate   The default discount rate.
         *                              Default: 1
         * @param minGamma              The minimum regularisation factor.
         *                              Default: 0.001
         * @param maxGamma              The maximum regularisation factor.
         *                              Default: 0.01
         * @param defaultGamma          The default regularisation factor.
         *                              Default: 0
         * @param minLambda             The minimum TD lambda.
         *                              Default: 0
         * @param maxLambda             The maximum TD lambda.
         *                              Default: 1
         * @param defaultLambda         The default TD lambda.
         *                              Default: 1
         */
        GameTester(Game* game, GamePlayer** player, const Str& filename,
                Vector* const* state, Vector* const* target,
                unsigned int* states, const unsigned int simulations = 10,
                const unsigned int iterations = 1000,
                LearnerType firstLearnerType = LearnerType::MCMCLearner,
                LearnerType lastLearnerType = LearnerType::TDNetLearner,
                LearnerType defaultLearnerType = LearnerType::TDNetLearner,
                unsigned int minHiddenSize = 5,
                unsigned int maxHiddenSize = 50,
                unsigned int defaultHiddenSize = 25,
                NeuronType firstNeuronType = NeuronType::Threshold,
                NeuronType lastNeuronType = NeuronType::HyperbolicTangent,
                NeuronType defaultNeuronType = NeuronType::Logistic,
                double minLearnRate = 0.001,
                double maxLearnRate = 1,
                double defaultLearnRate = 0.25,
                double minDiscountRate = 0,
                double maxDiscountRate = 1,
                double defaultDiscountRate = 1,
                double minGamma = 0.0001,
                double maxGamma = 0.1,
                double defaultGamma = 0,
                double minLambda = 0,
                double maxLambda = 1,
                double defaultLambda = 1);

        /**
         * Destructor.
         */
        virtual ~GameTester();

        /**
         * Sets a test to iterate through.
         *
         * @param gameTest  The test to enable or disable.
         * @param enable    Whether or not to enable the test. Default: True
         */
        void enableTest(GameTest gameTest, bool enable = true);

        /**
         * Loop through the enabled tests and outputs the data.
         */
        void testLoops();

        void setGame(Game* game);
        void setGamePlayers(GamePlayer** player);
        bool setOutputFile(const Str& filename);
        void setTestStates(Vector* const* state, unsigned int* states = 0);
        void setTestTargets(Vector* const* target, unsigned int* states = 0);
        void setSimulations(const unsigned int simulations);
        void setIterations(const unsigned int iterations);
        void setfirstLearnerType(const LearnerType firstLearnerType);
        void setLastLearnerType(const LearnerType lastLearnerType);
        void setDefaultLeanerType(const LearnerType defaultLearnerType);
        void setMinHiddenSize(const unsigned int minHiddenSize);
        void setMaxHiddenSize(const unsigned int maxHiddenSize);
        void setDefaultHiddenSize(const unsigned int defaultHiddenSize);
        void setFirstNeuronType(const NeuronType firstNeuronType);
        void setLastNeuronType(const NeuronType lastNeuronType);
        void setDefaultNeuronType(const NeuronType defaultNeuronType);
        void setMinLearnRate(const double minLearnRate);
        void setMaxLearnRate(const double maxLearnRate);
        void setDefaultLearnRate(const double defaultLearnRate);
        void setMinDiscountRate(const double minDiscountRate);
        void setMaxDiscountRate(const double maxDiscountRate);
        void setDefaultDiscountRate(const double defaultDiscountRate);
        void setMinGamma(const double minGamma);
        void setMaxGamma(const double maxGamma);
        void setDefaultGamma(const double defaultGamma);
        void setMinLambda(const double minLambda);
        void setMaxLambda(const double maxLambda);
        void setDefaultLambda(const double defaultLambda);

    private:
        Game* game;
        GamePlayer** player;
        Vector* const* state;
        Vector* const* target;
        unsigned int* states;

        unsigned int simulations;
        unsigned int iterations;

        LearnerType firstLearnerType;
        LearnerType lastLearnerType;
        LearnerType defaultLearnerType;
        unsigned int minHiddenSize;
        unsigned int maxHiddenSize;
        unsigned int defaultHiddenSize;
        NeuronType firstNeuronType;
        NeuronType lastNeuronType;
        NeuronType defaultNeuronType;
        double minLearnRate;
        double maxLearnRate;
        double defaultLearnRate;
        double minDiscountRate;
        double maxDiscountRate;
        double defaultDiscountRate;
        double minGamma;
        double maxGamma;
        double defaultGamma;
        double minLambda;
        double maxLambda;
        double defaultLambda;

        unsigned int players;
        unsigned int points;
        Matrix error;
        bool* test;

        std::ofstream file;
    };

    /**
     * Returns a string representing the name of the learner.
     *
     * @param type  The learner type to get the string representing the name of.
     * @return      The string representing the neame of the learner.
     */
    Str getLearnerName(const LearnerType type);

    /**
     * Assigns a new learner to a player.
     *
     * @param player        A pointer to the player to assign the new learner to.
     * @param learnerType   The type of learner to assign to the player.
     * @param learnRate     The learning rate of the learner.
     * @param discountRate  The discount rate of the learner.
     * @param hiddenSize    The size of the hidden layer for network learners.
     * @param hiddenType    The type of hidden layer neuron for network learners.
     * @param gamma         The regularisation factor for network learners.
     * @param lambda        The TD lambda value for the TD network learner.
     */
    void assignNewLearner(GamePlayer* player, const LearnerType learnerType,
            const double learnRate, const double discountRate,
            const unsigned int hiddenSize, const NeuronType hiddenType,
            const double gamma, const double lambda);

    /**
     * Players learn by playing each other in the game a number of times.
     *
     * @param game          Pointer to the game to play.
     * @param player        The array of pointers to the players.
     * @param iterations    The number of games to play.
     */
    void teachPlayers(Game* game, GamePlayer** player,
            const unsigned int iterations);

    /**
     * Tests the players.
     *
     * @param player    The array of pointers to the players.
     * @param players   The number of players.
     * @param state     The arrays of players' state vectors to test with.
     * @param target    The arrays of players' target vectors to test with.
     * @param states    The sizes of each players' test arrays.
     * @return          Returns the root mean square error.
     */
    double testPlayers(GamePlayer** player, const unsigned int players,
            const Vector* const* state, const Vector* const* target,
            const unsigned int* states);

    /**
     * Runs a simulation and records the test results.
     *
     * @param player        The array of pointers to the players.
     * @param players       The number of players.
     * @param error         The array to store the results in.
     * @param iterations    The number of games to play.
     * @param state         The arrays of players' state vectors to test with.
     * @param target        The arrays of players' target vectors to test with.
     * @param states        The sizes of each players' test arrays.
     */
    void runSimulation(Game* game, GamePlayer** player, double* error,
            const unsigned int iterations,
            const Vector* const* state, const Vector* const* target,
            const unsigned int* states);

    /**
     * Returns the number of test points a logarithmic increment of the
     * iterations will create.
     *
     * @param iterations    The total number of iterations.
     * @return              The number of points a logarithmic increment of the
     *                      iterations will create.
     */
    unsigned int getPoints(const unsigned int iterations);

    /**
     * Initialise the game tester results file.
     *
     * @param file          The file stream to initialise.
     * @param iterations    The total number of iterations.
     */
    void initialiseGameTestResultsFile(std::ofstream& file,
            const unsigned int iterations);
}

#endif // GAMETESTER_H
