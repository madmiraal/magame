# MAGame #

Cross-platform C++ code for creating automated multi-player games.

* ma::Game: Base class for creating automated multi-player games.
* ma::GameAction: Base class for creating game actions that can be processed by Game.
* ma::GameController: Game controller.
* ma::GameFrame: Game control frame.
* ma::GamePlayer: Game player.
* ma::GamePolicy: Base class for creating game policies.
    Sample policies:
    * ma::GreedyGamePolicy: Greedy policy.
    * ma::EGreedyGamePolicy: e-Greedy policy.
    * ma::ProbabilityGamePolicy: Probability policy.

## Dependencies ##
This code depends on:

* [wxWidgets](https://www.wxwidgets.org/)
* [MAUtils](https://bitbucket.org/madmiraal/mautils)
* [MAwxUtils](https://bitbucket.org/madmiraal/mawxutils)
* [MAANN](https://bitbucket.org/madmiraal/maann)
